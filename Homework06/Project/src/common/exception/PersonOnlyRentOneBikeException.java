package common.exception;

public class PersonOnlyRentOneBikeException extends EcobikeRentalException {
	private static final long serialVersionUID = 10913371361239062L;
	public PersonOnlyRentOneBikeException() {

	}
	
	public PersonOnlyRentOneBikeException(String message) {
		super(message);
	}
}
