package views.screen;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Random;
import java.util.logging.Logger;

import common.exception.MediaNotAvailableException;
import controller.StationInfoController;
import entity.Bike;
import entity.Station;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import utils.Configs;
import utils.Utils;
import views.screen.FXMLScreenHandler;
import views.screen.HomeScreenHandler;
import views.screen.station.StationScreenHandler;

public class BikeDetailAdminHandler extends FXMLScreenHandler{
	
	public static Logger LOGGER = Utils.getLogger(BikeDetailAdminHandler.class.getName());
	@FXML
	private ImageView imageBike;
	
	@FXML
	private Label nameBike;
	
	@FXML
	private Label typeName;
	
	@FXML
	private Label Deposit;
	
	@FXML
	private Label licensePlate;
	
	@FXML
	private Label status;
	
	
	
	public BikeDetailAdminHandler(String screenPath, Bike bike, AdminBikeHandler adminbikehandler) throws SQLException, IOException {
		super(screenPath);
		nameBike.setText(bike.getName());
		typeName.setText(bike.getBikeType().getTypeName());
		Deposit.setText(String.valueOf(bike.getBikeType().getDeposit()) + " VNĐ");
		licensePlate.setText(bike.getLicencePlate());
		File file1 = new File(Configs.IMAGE_PATH + "/" + "xedap1.jpg");
		File file2 = new File(Configs.IMAGE_PATH + "/" + "xedap2.jpg");
		File file3 = new File(Configs.IMAGE_PATH + "/" + "xedap3.jpg");
		File file4 = new File(Configs.IMAGE_PATH + "/" + "xedap4.jpg");
		File file5 = new File(Configs.IMAGE_PATH + "/" + "xedap5.jpg");
        Image img1 = new Image(file1.toURI().toString());
        Image img2 = new Image(file2.toURI().toString());
        Image img3 = new Image(file3.toURI().toString());
        Image img4 = new Image(file4.toURI().toString());
        Image img5 = new Image(file5.toURI().toString());
        if(bike.getId()%5<1){
        	imageBike.setImage(img1);
        }
        else if(bike.getId()%5<2){
        	imageBike.setImage(img2);
        }
        if(bike.getId()%5<3){
        	imageBike.setImage(img3);
        }
        else if(bike.getId()%5<4){
        	imageBike.setImage(img4);
        }
        else {
        	imageBike.setImage(img5);
        }
        
		if (bike.getStatus() == 0) {
			status.setText("CAN NOT RENT");
		} else {
			status.setText("Avaivable");
		}
	}
	

	@FXML
	void editBike(MouseEvent event) throws IOException {
		System.out.println("Edit Bike");
	}
	

}
