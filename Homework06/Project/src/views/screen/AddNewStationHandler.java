package views.screen;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;
import java.util.logging.Logger;

import controller.AdminController;
import controller.PaymentController;
import controller.StationController;
import controller.StationInfoController;
import entity.Station;
import java.io.IOException;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import utils.Configs;
import utils.Utils;
import views.screen.AdminStationHandler;
import views.screen.BaseScreenHandler;
import views.screen.popup.PopupScreen;
public class AddNewStationHandler extends BaseScreenHandler {
	
	public static Logger LOGGER = Utils.getLogger(HomeScreenHandler.class.getName());
	
	@FXML
	private TextField name;
	
	@FXML
	private TextField stationAddress;
	
	@FXML
	private TextField emptySlot;
	
	@FXML
	private Button btnAddNew;

	public AddNewStationHandler(Stage stage, String screenPath) throws IOException {
		super(stage, screenPath);
		
		btnAddNew.setOnMouseClicked(e -> {
			try {
				requestToAddNewStation();				
			}catch (IOException | SQLException exp) {
				exp.getStackTrace();
			}
		});
	}
	
	public StationController getBController() {
		return (StationController) super.getBController();
	}

	public void requestToViewAddNewStation(BaseScreenHandler prevScreen) throws SQLException {
		setPreviousScreen(prevScreen);
		setScreenTitle("Add new station screen");
		show();
	}
	
	public void requestToAddNewStation() throws IOException, SQLException {
		String contents = "content";
		LOGGER.info("Click add new");
		if (!checkInfo()) {
			return;
		}
		StationController stationController = (StationController) getBController();
		Map<String, String> response = stationController.addNewStation(name.getText(), stationAddress.getText(), emptySlot.getText());
		
		AdminStationHandler adminStation;
		LOGGER.info("Station button clicked");
		adminStation = new AdminStationHandler(this.stage, Configs.ADMIN_STATION_SCREEN_PATH);
//		adminStation.setHomeScreenHandler(this);
		adminStation.setBController(new StationController());
		adminStation.requestToViewAdminStation(this);
	}
	
	boolean checkInfo() {
		StationController stationController = (StationController) getBController();
		String message = stationController.validateStation(name.getText(), stationAddress.getText(), emptySlot.getText());
		if(!message.equals("OK")) {
			showAlert(message);
			return false;
		}
		return true;
	}
	
	void showAlert(String message) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Th�ng b�o");
        alert.setHeaderText("");
        alert.setContentText(message);
        alert.showAndWait();
    }
}
