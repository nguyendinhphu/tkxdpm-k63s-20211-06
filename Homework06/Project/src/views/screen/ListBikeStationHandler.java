package views.screen;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import common.exception.ViewCartException;
import controller.AdminController;
import controller.BikeController;
import controller.HomeController;
import controller.PaymentController;
import controller.StationController;
import entity.Bike;
import entity.Station;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import utils.Configs;
import utils.Utils;
import views.screen.BaseScreenHandler;
import views.screen.popup.PopupScreen;

public class ListBikeStationHandler extends BaseScreenHandler implements Initializable {
	
	public static Logger LOGGER = Utils.getLogger(ListBikeStationHandler.class.getName());
	
	@FXML
	private ImageView aimsImage;
	
	@FXML
	private Button btnsearch;
	
	
	@FXML
	private HBox hboxMedia;
	
	@FXML
	private VBox vboxMedia1;
	
	@FXML
	private VBox vboxMedia2;
	
	@FXML
	private VBox vboxMedia3;
	
	private List homeItems;
	
	public ListBikeStationHandler(Stage stage, String screenPath) throws IOException {
		super(stage, screenPath);
		// TODO Auto-generated constructor stub
	}
	
	
	public HomeController getBController() {
		return (HomeController) super.getBController();
	}
	
	public void init(Station station) {
		setBController(new HomeController());
		try {
			List medium = getBController().getListBikeByStationId(station.getId());
			this.homeItems = new ArrayList<>();
			for (Object object : medium) {
                Bike bike = (Bike)object;
                BikeDetailHomeHandler m1 = new BikeDetailHomeHandler(Configs.BIKE_DETAIL_HOME, bike, this);
                this.homeItems.add(m1);
			}
			addBikeHome(this.homeItems);
		}catch (SQLException | IOException e){
            LOGGER.info("Errors occured: " + e.getMessage());
            e.printStackTrace();
        }
		
		aimsImage.setOnMouseClicked(e -> {
//			homeScreenHandler.show();
		});
	}
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
//		setBController(new HomeController());
//		try {
//			List medium = getBController().getListAllBike();
//			this.homeItems = new ArrayList<>();
//			for (Object object : medium) {
//                Bike bike = (Bike)object;
//                BikeDetailHomeHandler m1 = new BikeDetailHomeHandler(Configs.BIKE_DETAIL_ADMIN, bike, this);
//                this.homeItems.add(m1);
//			}
//			addBikeHome(this.homeItems);
//		}catch (SQLException | IOException e){
//            LOGGER.info("Errors occured: " + e.getMessage());
//            e.printStackTrace();
//        }
//		
//		aimsImage.setOnMouseClicked(e -> {
//            
//        });
//		
//
//		
	}
	
	public void setImage() {
		// fix image path caused by fxml
        File file1 = new File(Configs.IMAGE_PATH + "/" + "Logo.png");
        Image img1 = new Image(file1.toURI().toString());
        aimsImage.setImage(img1);
	}
	
	public void addBikeHome(List items) {
		ArrayList bikeItems = (ArrayList)((ArrayList) items).clone();
        hboxMedia.getChildren().forEach(node -> {
            VBox vBox = (VBox) node;
            vBox.getChildren().clear();
        });
        while(!bikeItems.isEmpty()){
            hboxMedia.getChildren().forEach(node -> {
                int vid = hboxMedia.getChildren().indexOf(node);
                VBox vBox = (VBox) node;
                while(vBox.getChildren().size()<3 && !bikeItems.isEmpty()){
                	BikeDetailHomeHandler media = (BikeDetailHomeHandler) bikeItems.get(0);
                    vBox.getChildren().add(media.getContent());
                    bikeItems.remove(media);
                }
            });
            return;
        }
	}
	
	public void requestToViewAdminStation(BaseScreenHandler prevScreen) throws SQLException {
		setPreviousScreen(prevScreen);
		setScreenTitle("Admin Station Screen");
//		addStationHome(this.homeItems);
		show();
	}


}
