package views.screen;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Random;
import java.util.logging.Logger;

import common.exception.MediaNotAvailableException;
import controller.BikeController;
import controller.StationController;
import controller.StationInfoController;
import entity.Bike;
import entity.Station;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import utils.Configs;
import utils.Utils;
import views.screen.FXMLScreenHandler;
import views.screen.HomeScreenHandler;
import views.screen.station.StationScreenHandler;

public class BikeDetailHomeHandler extends FXMLScreenHandler{
	
	public static Logger LOGGER = Utils.getLogger(BikeDetailHomeHandler.class.getName());
	@FXML
	private ImageView imageBike;
	
	@FXML
	private Label nameBike;
	
	@FXML
	private Label typeName;
	
	@FXML
	private Label Deposit;
	
	@FXML
	private Label licensePlate;
	
	@FXML
	private Label status;
	
	@FXML
	private Button info;
	
	public BikeDetailHomeHandler(String screenPath, Bike bike, ListBikeStationHandler listBikeStationHandler) throws SQLException, IOException {
		super(screenPath);
		nameBike.setText(bike.getName());
		typeName.setText(bike.getBikeType().getTypeName());
		Deposit.setText(String.valueOf(bike.getBikeType().getDeposit()) + " VNĐ");
		licensePlate.setText(bike.getLicencePlate());

		File file = new File(bike.getImage());
		Image image = new Image(file.toURI().toString());
		imageBike.setImage(image);
        status.setText(bike.getStatusName(bike.getStatus()));

		info.setOnMouseClicked(e -> {	
			Stage stage;
	        stage=(Stage) info.getScene().getWindow();
			BikeInfoScreenHandler addNewStation;
			try {
				addNewStation = new BikeInfoScreenHandler(stage, Configs.BIKE_INFO_PATH,bike);
//				addNewStation.setHomeScreenHandler(homeScreenHandler);
				addNewStation.setBController(new StationController());
				addNewStation.show();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
        });
		
	}

}
