package service;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Logger;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import property.Properties;
import utils.Utils;
import entity.RentalDeal;
import entity.Station;
  
public class RentalDealService {
	private static Logger LOGGER = Utils.getLogger(RentalDealService.class.getName());
	
	public boolean createRentalDeal(RentalDeal rentalDeal) throws SQLException {
		//get format time
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		String sql = "Insert into RentalDeal(bikeId, userId, beginningTime, cardNumber , status) values ("
				+ rentalDeal.getBike().getId() + " ,"
				+ rentalDeal.getUser().getId() + " ,\""
				+ timeStamp + "\" , \"" +
				rentalDeal.getCardNumber() + "\" , 1 )" ;

		Statement stm = Properties.getConnection().createStatement();
        return stm.execute(sql);
        
	}
	
	public boolean checkUserCanRentBike(int userId) throws SQLException {
		String sql = " Select * from RentalDeal WHERE userId= " + userId + " and status = 1;";
		System.out.print(sql);
		Statement stm = Properties.getConnection().createStatement();
		ResultSet res = stm.executeQuery(sql);
		while (!res.next()) {
            return true;
        }
		return false;
	}
		
}
