package service;

import entity.Station;
import entity.Bike;
import entity.BikeType;
import service.BikeService;
import utils.Utils;
import views.screen.HomeScreenHandler;
import property.Properties;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;


public class StationService {
	
	public static Logger LOGGER = Utils.getLogger(HomeScreenHandler.class.getName());
	
	//add new station
	public boolean addNewStation(String name, String location, String emptySlot) throws SQLException {
		Connection connection = Properties.getConnection();
		String query = "insert into Station (name, location, emptySlot)" + "values (?, ?, ?)";
		PreparedStatement preparedStatement =  connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
		preparedStatement.setString(1, name);
		preparedStatement.setString(2, location);
		preparedStatement.setString(3, emptySlot);
		LOGGER.info("namee");
	//	preparedStatement.setString(3,st.getImage()); can function rieng
		
		preparedStatement.executeUpdate();
		ResultSet res = preparedStatement.getGeneratedKeys();
	//	connection.close();
		return true;	
	}
	
	// edit station info
		public boolean updateStation(String id, String stationName, String stationAddress, 
				 String bikesNumber, String ebikesNumber, 
				 String twinbikesNumber, String vacanciesNumber) throws SQLException {
			Connection connection = Properties.getConnection();
			String query = "update Station set name=?, location=?, bikeQuantity=?, eBikeQuantity=?, twinBikeQuantity=?, emptySlot=? where id=?";
			PreparedStatement preparedStatement =  connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1, stationName);
			preparedStatement.setString(2, stationAddress);
			preparedStatement.setString(3, bikesNumber);
			preparedStatement.setString(4, ebikesNumber);
			preparedStatement.setString(5, twinbikesNumber);
			preparedStatement.setString(6, vacanciesNumber);
			preparedStatement.setString(7, id);
		//	preparedStatement.setString(3,st.getImage()); can function rieng
			
			preparedStatement.executeUpdate();
			ResultSet res = preparedStatement.getGeneratedKeys();
//			connection.close();
			return true;	
		}
	
	//get bike quantity
		public int getBikeQuantity() throws SQLException {
			return 0;
			
		}
	
	//get list station (not finished)
	public List getListAllStation() throws SQLException {
		Statement stm = Properties.getConnection().createStatement();
        ResultSet res = stm.executeQuery("select * from station");
        ArrayList medium = new ArrayList<>();
        while (res.next()) {
            Station station = new Station();
            int id = res.getInt("id");
                station.setId(id);
                station.setName(res.getString("name"));
                station.setLocation(res.getString("location"));
                station.setBikeQuantity(countBike(id,1));
                station.seteBikeQuantity(countBike(id,2));
                station.setTwinBikeQuantity(countBike(id,3));
                station.setEmptySlot(res.getInt("emptySlot"));
            medium.add(station);
        }
        
        return medium;
	}
	
	public int countBike(int stationId, int typeId) throws SQLException{
		Connection connection = Properties.getConnection();
		String query = "select count(*) as count from Bike where stationId=? and typeId=? ";
		PreparedStatement preparedStatement =  connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
		preparedStatement.setInt(1, stationId);
		preparedStatement.setInt(2, typeId);
		ResultSet res = preparedStatement.executeQuery();
		res.next();
		int count = res.getInt("count");
		
		return count;	
	}
	public Station getStationById(int stationId) throws SQLException {
		String sql = " Select * from station WHERE id=" + stationId;
		Statement stm = Properties.getConnection().createStatement();
		ResultSet res = stm.executeQuery(sql);
		Station station = new Station();
		
		if(res.next()) {
			  station.setBikeQuantity(res.getInt("bikeQuantity"));
			  station.seteBikeQuantity(res.getInt("eBikeQuantity"));
			  station.setTwinBikeQuantity(res.getInt("twinBikeQuantity"));
			  station.setId(res.getInt("id"));
	          station.setName(res.getString("name"));
	          station.setLocation(res.getString("location"));
	          station.setEmptySlot(res.getInt("emptySlot"));
	          return station;
      }
		return null;
	}
	
	public int updateInfoStation(int stationId, int typeBikeId) throws SQLException {
		Statement stm = Properties.getConnection().createStatement();
        Station station = getStationById(stationId);
    
        if (station != null) {
        	int emptySlot = station.getEmptySlot();
        	emptySlot++;
        	int bikeQuantity = station.getBikeQuantity();
            int eBikeQuantity = station.geteBikeQuantity();
            int twinBikeQuantity = station.getTwinBikeQuantity();
            switch (typeBikeId) {
	    		case BikeType.BIKE:
	    			bikeQuantity = (bikeQuantity > 0) ? bikeQuantity - 1  : bikeQuantity;
	    			break;
	    		case BikeType.EBIKE:
	    			eBikeQuantity = (eBikeQuantity > 0) ? eBikeQuantity - 1  : eBikeQuantity;
	    			break;
	    		case BikeType.TWIN_BIKE:
	    			twinBikeQuantity = (twinBikeQuantity > 0) ? twinBikeQuantity - 1  : eBikeQuantity;
	    			break;
            }

            return stm.executeUpdate("update station set bikeQuantity = " + bikeQuantity +" , "
            		+ "twinBikeQuantity = " + twinBikeQuantity 
            		+ ", emptySlot = " + emptySlot
                    + ", eBikeQuantity = "  + eBikeQuantity 
                    + " where id=" + stationId + ";"
              );
   
        }
        return 0;
	}

	public List getListAllStationExistEmpty() throws SQLException {
		Statement stm = Properties.getConnection().createStatement();
        ResultSet res = stm.executeQuery("select * from station where emptySlot>0");
        ArrayList medium = new ArrayList<>();
        while (res.next()) {
            Station station = new Station();
                station.setId(res.getInt("id"));
                station.setName(res.getString("name"));
                station.setLocation(res.getString("location"));
                
            medium.add(station);
        }
        return medium;
	}




}
