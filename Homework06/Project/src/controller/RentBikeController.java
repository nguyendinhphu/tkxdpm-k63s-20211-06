package controller;

import entity.Bike;
import entity.RentalDeal;
import entity.User;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import common.exception.BikeNotAvaibilableException;
import common.exception.PersonOnlyRentOneBikeException;
import service.BikeService;
import service.RentalDealService;
import service.StationService;
public class RentBikeController extends BaseController {
	private BikeService bikeService;
	private StationService stationService;
	private RentalDealService rentalDealService;
	
	public RentBikeController() {
		bikeService = new BikeService();
		stationService = new StationService();
		rentalDealService = new RentalDealService();
	}
	
	public boolean requestToRentBike(Bike bike, User user) {
		if (!bikeService.checkBikeAvaibility(bike)) {
            throw new BikeNotAvaibilableException("The bike can not rented.");
        }
		boolean check = false;
		
		try {
			check = rentalDealService.checkUserCanRentBike(user.getId());
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		if (!check) throw new PersonOnlyRentOneBikeException("One person can only rent one bicycle!");
		
        return true;
	}
	
	public void confirmRentBike() {
		
	}
	
	
    public RentalDeal createRentalDeal(Bike bike, User user) {
        RentalDeal rentalDeal = new RentalDeal(bike);
        rentalDeal.setBike(bike);
        rentalDeal.setStatus(RentalDeal.STATUS_NOT_RETURNED);
        rentalDeal.setBeginingTime(Timestamp.valueOf(LocalDateTime.now()));
        rentalDeal.setUser(user);
        return rentalDeal;
    }
    
    public int updateBikeStatus(Bike bike) throws SQLException {
    	return bikeService.updateBikeStatusById(bike.getId(), Bike.NOT_AVAILABLE);
    }

    public int updateInfoStation(int stationId, int bikeTypeId) throws SQLException {
    	return stationService.updateInfoStation(stationId, bikeTypeId);
    }
    
    
}
