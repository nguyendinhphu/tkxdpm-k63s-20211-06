package controller;

import java.sql.SQLException;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import common.exception.PaymentException;
import common.exception.UnrecognizedException;
import entity.Station;
import service.StationService;
import utils.Utils;
import views.screen.HomeScreenHandler;

public class StationController extends BaseController {
	
	public static Logger LOGGER = Utils.getLogger(HomeScreenHandler.class.getName());
	
	public List getListStation() throws SQLException {
		return new StationService().getListAllStation();		
	}
	
	public Map<String, String> addNewStation(String name, String location, String emptySlot) {
		
		Map<String, String> result = new Hashtable<String, String>();
		LOGGER.info("hjdjf");
		result.put("RESULT", "UPDATE FAILED!");
		try {
			LOGGER.info("nbfnf");
			StationService service = new StationService();
			boolean success = service.addNewStation(name, location, emptySlot);
			result.put("RESULT", "UPDATE SUCCESSFUL!");
			result.put("MESSAGE", "You have succesffully update station info!");
		} catch (PaymentException | UnrecognizedException | SQLException ex) {
			LOGGER.info("catch");
			ex.printStackTrace();
			result.put("MESSAGE", ex.getMessage());
		}
		return result;		
	}
	
	public boolean validateInteger(String number) {
    	//check number starts with 0
    	if(number == null) return false;
    	// check phoneNumber contains only number
    	try {
    		int num = Integer.parseInt(number);
    		if(num < 0) return false;
    	}catch (NumberFormatException e) {
    		return false;
    	}
    	return true;
    }
	
    public boolean ValidateString (String string) {
    	if(string == null) return false;
    	Pattern pattern = Pattern.compile("[^A-Za-z0-9]");
    	Matcher matcher = pattern.matcher(string.trim().replaceAll("\\s",""));
        if(matcher.find()) return false;
    	return true;
    }
    
    /**
     * Method validate station name
     * @param name station name
     * @return true/false
     */
    public boolean validateStationName(String name) {
    	return ValidateString(name);
    }
    
    /**
     * @param address to validate
     * @return true/false
     */
    public boolean validateAddress(String address) {
    	return ValidateString(address);
    }
    
    public boolean validateEmptySlotNumber(String number) {
    	return validateInteger(number);
    }
    
    public String validateStation(String name, String location, String emptySlot) {
    	if(name.equals("")) return "Vui lòng nhập tên bãi xe";
    	else if(!validateStationName(name)) return "Vui lòng nhập lại tên bãi xe.";
    	
    	if(location.equals("")) return "Vui lòng nhập địa chỉ bãi xe";
    	else if(!validateAddress(location)) return "Vui lòng nhập lại địa chỉ bãi xe.";
    	
    	if(emptySlot.equals("")) return "Vui lòng nhập số lượng vị trí trống";
    	else if(!validateEmptySlotNumber(emptySlot)) return "Vui lòng nhập lại số lượng vị trí trống.";
    	return "OK";
    }

}
