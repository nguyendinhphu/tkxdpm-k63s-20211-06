package controller;

import java.sql.SQLException;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import entity.RentalDeal;
import service.GiveBackService;

public class GiveBackBikeController extends BaseController {
	private static Logger LOGGER = utils.Utils.getLogger(GiveBackBikeController.class.getName());

	public boolean getRentalBike() throws SQLException {
		GiveBackService giveBackService = new GiveBackService();
		Boolean avail = giveBackService.checkAvailableRentDeal(1);
		return avail;
	}

	public RentalDeal createRentalDetal() throws SQLException {
		GiveBackService giveBackService = new GiveBackService();
		RentalDeal rd = giveBackService.getRentalDeal(1);

		return rd;
	}

	public double calculateFee(RentalDeal rentalDeal) {
		long min = (rentalDeal.getReturnedTime().getTime() - rentalDeal.getBeginingTime().getTime()) / 1000 / 60;
		int deposit = rentalDeal.getBike().getBikeType().getDeposit();
		double fee = 0;
		if (min <= 10) {
			fee = 0;
		} else if (min <= 30) {
			fee = 10000;
		} else {
			if (min % 15 == 0) {
				fee = 10000 + (min - 30) / 15 * 3000;
			} else {
				fee = 13000 + (min - 30) / 15 * 3000;
			}

			// neu la loai xe khac
		}
		if (deposit != 400000) {
			fee = fee * 1.5;
		}
		fee = deposit - fee;
		return fee;
	}

	public boolean validateIsNumber(String number) {
		try {
			Integer.parseInt(number);
		} catch (NumberFormatException e) {
			return false;
		}
		return true;
	}

	public boolean validateName(String name) {
		String specialCharacters = "!#$%&'()*+,-./:;<=>?@[]^_`{|}~0123456789";
		String[] strlCharactersArray = new String[name.length()];
		for (int i = 0; i < name.length(); i++) {
			strlCharactersArray[i] = Character.toString(name.charAt(i));
		}

		int count = 0;
		for (int i = 0; i < strlCharactersArray.length; i++) {
			if (specialCharacters.contains(strlCharactersArray[i])) {
				count++;
			}

		}

		if (name != null && count == 0) {
			return true;
		} else {
			return false;
		}

	}

	public boolean validateOwner(String name) {
		if (name == null)
			return false;
		Pattern pattern = Pattern.compile("[^A-Za-z0-9]");
		Matcher matcher = pattern.matcher(name.trim().replaceAll("\\s", ""));
		if (matcher.find())
			return false;
		return true;

	}

	public boolean validateCreditCard(String name) {
		return true;
	}

	public boolean validateExpireDate(String dateString) {
		if (dateString.length() != 5 || dateString == null) {
			return false;
		}
		if (dateString.charAt(2) != '/') {
			return false;
		}
		try {
			Integer.parseInt(dateString.substring(0,2));
			Integer.parseInt(dateString.substring(3));
			if (Integer.parseInt(dateString.substring(0,2)) > 12 || Integer.parseInt(dateString.substring(0,2)) < 1) {
				return false;
			}
		} catch (NumberFormatException e) {
			return false;
		}
		
			
			
		
		return true;
	}
	public boolean validateCodeSecurity(String code) {
		if (code.length() != 3 || code == null) {
			return false;
		}
		try {
			Integer.parseInt(code);
		} catch (NumberFormatException e) {
			// TODO: handle exception
			return false;
		}
		return true;
	}
	
	
	public String validateInfoPayment(String cardNumber, String fullName, String expiredDate, String codeSecurity, String nameBank) {
		// TODO Auto-generated method stub
		if (!validateNotNull(cardNumber, fullName, expiredDate, codeSecurity, nameBank)) {
			return "Các trường không được phép để trống";
		}
		if (!validateOwner(fullName)) {
			return "Tên không được chứa kí tự đặc biệt";
		}
		if (!validateExpireDate(expiredDate)) {
			return "Ngày hết hạn phải theo định dạng MM/YY";
		}
		if (!validateCodeSecurity(codeSecurity)) {
			return "CVV chỉ có 3 kí tự là số, vui lòng nhập lại";
		}
		return "OK";

	}
	private boolean validateNotNull(String cardNumber, String fullName, String expiredDate, String codeSecurity, String nameBank) {
		if (cardNumber == null || cardNumber == "") {
			return false;
		}
		if (fullName == null || fullName == "") {
			return false;
		}
		if (expiredDate == null || expiredDate == "") {
			return false;
		}
		if (codeSecurity == null || codeSecurity == "") {
			return false;
		}
		if (nameBank == null) {
			return false;
		}
		return true;
	}

}
