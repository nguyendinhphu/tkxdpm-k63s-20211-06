package controller;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class ValidateOwnerString {

	private GiveBackBikeController giveBackBikeController;

	@BeforeEach
	void setUp() throws Exception {
		giveBackBikeController = new GiveBackBikeController();
	}

	@ParameterizedTest
	@CsvSource({
		"Ha Noi, true",
		"20/11 Van Don, false",
		"108 Hai$ Ba Tru%g, false"
	})
	
	void test(String address, boolean expected) {
		//when
		boolean isValided = giveBackBikeController.validateOwner(address);
		//then 
		assertEquals(expected, isValided);
	}
}
