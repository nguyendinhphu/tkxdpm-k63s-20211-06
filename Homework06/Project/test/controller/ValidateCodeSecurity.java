package controller;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class ValidateCodeSecurity {

	private GiveBackBikeController giveBackBikeController;

	@BeforeEach
	void setUp() throws Exception {
		giveBackBikeController = new GiveBackBikeController();
	}

	@ParameterizedTest
	@CsvSource({
		"124, true",
		"2011 , false",
		"1a2, false"
	})
	
	void test(String codeString, boolean expected) {
		//when
		boolean isValided = giveBackBikeController.validateCodeSecurity(codeString);
		//then 
		assertEquals(expected, isValided);
	}

}
