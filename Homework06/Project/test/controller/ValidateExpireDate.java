package controller;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class ValidateExpireDate {

	private GiveBackBikeController giveBackBikeController;

	@BeforeEach
	void setUp() throws Exception {
		giveBackBikeController = new GiveBackBikeController();
	}

	@ParameterizedTest
	@CsvSource({
		"11/24, true",
		"20-11 , false",
		"108/22, false"
	})
	
	void test(String dateString, boolean expected) {
		//when
		boolean isValided = giveBackBikeController.validateExpireDate(dateString);
		//then 
		assertEquals(expected, isValided);
	}
}
