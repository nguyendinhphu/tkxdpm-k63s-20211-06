package service;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import entity.Bike;

/**
 * 
 * @author TuyenTV_20184012
 *
 */
class checkBikeAvaibilityTest {

	private BikeService bikeService;
	private Bike bike;
	@BeforeEach
	void setUp() throws Exception {
		bikeService = new BikeService();
		bike = new Bike();
	}
	@ParameterizedTest
	@CsvSource({
		"1 , true",
		"0 , false"
	})

	
	void test(int status, boolean expected) {
		bike.setStatus(status);
		boolean isValid = bikeService.checkBikeAvaibility(bike);
		assertEquals((boolean)(expected), (boolean)isValid);
	}
	

}